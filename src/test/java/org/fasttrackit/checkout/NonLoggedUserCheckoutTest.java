package org.fasttrackit.checkout;

import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.fasttrackit.*;
import org.fasttrackit.config.BaseTestConfig;
import org.fasttrackit.data.provider.ProductsDataProvider;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

@Feature("Checkout - Non Logged User")
public class NonLoggedUserCheckoutTest extends BaseTestConfig {
    DemoShopPage page;

    @BeforeMethod
    public void openDemoShopPage() {
        this.page = new DemoShopPage();
        page.openDemoShopApp();
    }

    @AfterMethod
    public void cleanup() {
        System.out.println("Cleaning up after the test");
        Footer footer = new Footer();
        footer.resetPage();
    }

    @Test(dataProviderClass = ProductsDataProvider.class, dataProvider = "productsDataProvider")
    @Severity(SeverityLevel.BLOCKER)
    @Description("A non logged user performs checkout with one product in cart.")
    public void non_logged_user_performs_checkout_with_one_product(Product product) {
        product.addToCart();
        page.getHeader().clickOnTheCartIcon();
        CartPage cartPage = new CartPage();
        cartPage.clickOnTheCheckoutButton();
        CheckOutPage checkoutPage = new CheckOutPage();
        checkoutPage.fillInFirstName("Felicia");
        checkoutPage.fillInLastName("Morales");
        checkoutPage.fillInAddress("Iatly, Rome, Portico 19");
        checkoutPage.clickOnContinueCheckoutButton();
        checkoutPage.clickOnContinueCheckoutButton();

        String completeOrderMessage = checkoutPage.getCompleteOrderMessage();
        assertEquals(completeOrderMessage, "Thank you for your order!", "Complete order message is: " + completeOrderMessage);
    }

    @Test(dataProviderClass = ProductsDataProvider.class, dataProvider = "productsDataProvider")
    @Severity(SeverityLevel.BLOCKER)
    @Description("A non logged user performs checkout with two products in cart.")
    public void non_logged_user_performs_checkout_with_two_products(Product product) {
        product.addToCart();
        page.getHeader().clickOnTheCartIcon();
        CartPage cartPage = new CartPage();
        cartPage.clickOnContinuesShoppingButton();
        product.addToCart();
        page.getHeader().clickOnTheCartIcon();
        cartPage.clickOnTheCheckoutButton();
        CheckOutPage checkoutPage = new CheckOutPage();
        checkoutPage.fillInFirstName("Felicia");
        checkoutPage.fillInLastName("Morales");
        checkoutPage.fillInAddress("Iatly, Rome, Portico 19");
        checkoutPage.clickOnContinueCheckoutButton();
        checkoutPage.clickOnContinueCheckoutButton();

        String completeOrderMessage = checkoutPage.getCompleteOrderMessage();
        assertEquals(completeOrderMessage, "Thank you for your order!", "Complete order message is: " + completeOrderMessage);
    }

    @Test(dataProviderClass = ProductsDataProvider.class, dataProvider = "productsDataProvider")
    @Severity(SeverityLevel.BLOCKER)
    @Description("A non logged user performs checkout without checkout information.")
    public void non_logged_user_performs_checkout_without_any_addressInformation(Product product) {
        product.addToCart();
        page.getHeader().clickOnTheCartIcon();
        CartPage cartPage = new CartPage();
        cartPage.clickOnTheCheckoutButton();
        CheckOutPage checkoutPage = new CheckOutPage();
        checkoutPage.clickOnContinueCheckoutButton();

        String errorMessage = checkoutPage.getErrorMessage();
        assertEquals(errorMessage, "First Name is required", "Error message is: " + errorMessage);
    }

    @Test(dataProviderClass = ProductsDataProvider.class, dataProvider = "productsDataProvider")
    @Severity(SeverityLevel.BLOCKER)
    @Description("A non logged user performs checkout without first name.")
    public void non_logged_user_performs_checkout_without_first_name(Product product) {
        product.addToCart();
        page.getHeader().clickOnTheCartIcon();
        CartPage cartPage = new CartPage();
        cartPage.clickOnTheCheckoutButton();
        CheckOutPage checkoutPage = new CheckOutPage();
        checkoutPage.fillInFirstName("");
        checkoutPage.fillInLastName("Morales");
        checkoutPage.fillInAddress("Iatly, Rome, Portico 19");
        checkoutPage.clickOnContinueCheckoutButton();

        String errorMessage = checkoutPage.getErrorMessage();
        assertEquals(errorMessage, "First Name is required", "Error message is: " + errorMessage);
    }

    @Test(dataProviderClass = ProductsDataProvider.class, dataProvider = "productsDataProvider")
    @Severity(SeverityLevel.BLOCKER)
    @Description("A non logged user performs checkout without last name.")
    public void non_logged_user_performs_checkout_without_last_name(Product product) {
        product.addToCart();
        page.getHeader().clickOnTheCartIcon();
        CartPage cartPage = new CartPage();
        cartPage.clickOnTheCheckoutButton();
        CheckOutPage checkoutPage = new CheckOutPage();
        checkoutPage.fillInFirstName("Felicia");
        checkoutPage.fillInLastName("");
        checkoutPage.fillInAddress("Iatly, Rome, Portico 19");
        checkoutPage.clickOnContinueCheckoutButton();

        String errorMessage = checkoutPage.getErrorMessage();
        assertEquals(errorMessage, "Last Name is required", "Error message is: " + errorMessage);
    }

    @Test(dataProviderClass = ProductsDataProvider.class, dataProvider = "productsDataProvider")
    @Severity(SeverityLevel.BLOCKER)
    @Description("A non logged user performs checkout without address.")
    public void non_logged_user_performs_checkout_without_address(Product product) {
        product.addToCart();
        page.getHeader().clickOnTheCartIcon();
        CartPage cartPage = new CartPage();
        cartPage.clickOnTheCheckoutButton();
        CheckOutPage checkoutPage = new CheckOutPage();
        checkoutPage.fillInFirstName("Felicia");
        checkoutPage.fillInLastName("Morales");
        checkoutPage.fillInAddress("");
        checkoutPage.clickOnContinueCheckoutButton();

        String errorMessage = checkoutPage.getErrorMessage();
        assertEquals(errorMessage, "Address is required", "Error message is: " + errorMessage);
    }

    @Test(dataProviderClass = ProductsDataProvider.class, dataProvider = "productsDataProvider")
    @Severity(SeverityLevel.BLOCKER)
    @Description("A non logged user cancel checkout before fill in checkout information.")
    public void non_logged_user_cancel_checkout_before_fill_in_information(Product product) {
        product.addToCart();
        page.getHeader().clickOnTheCartIcon();
        CartPage cartPage = new CartPage();
        cartPage.clickOnTheCheckoutButton();
        CheckOutPage checkoutPage = new CheckOutPage();
        checkoutPage.clickOnCancelButton();

        String text = String.valueOf(cartPage);
        String expected = text.substring(0, 24);
        assertEquals(expected, "org.fasttrackit.CartPage", "The shopping process return to cart page");
    }

    @Test(dataProviderClass = ProductsDataProvider.class, dataProvider = "productsDataProvider")
    @Severity(SeverityLevel.BLOCKER)
    @Description("A non logged user cancel checkout after fill in checkout information.")
    public void non_logged_user_cancel_checkout_after_fill_in_information(Product product) {
        product.addToCart();
        page.getHeader().clickOnTheCartIcon();
        CartPage cartPage = new CartPage();
        cartPage.clickOnTheCheckoutButton();
        CheckOutPage checkoutPage = new CheckOutPage();
        checkoutPage.fillInFirstName("Felicia");
        checkoutPage.fillInLastName("Morales");
        checkoutPage.fillInAddress("Iatly, Rome, Portico 19");
        checkoutPage.clickOnContinueCheckoutButton();
        checkoutPage.clickOnCancelButton();

        String text = String.valueOf(cartPage);
        String expected = text.substring(0, 24);
        assertEquals(expected, "org.fasttrackit.CartPage", "The shopping process return to cart page");
    }
}