package org.fasttrackit.checkout;

import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.fasttrackit.*;
import org.fasttrackit.config.BaseTestConfig;
import org.fasttrackit.data.provider.User;
import org.fasttrackit.data.provider.UserDataProvider;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

@Feature("Checkout - Logged User")
public class LoggedUserCheckoutTest extends BaseTestConfig {
    DemoShopPage page;

    @BeforeMethod
    public void openDemoShopPage() {
        this.page = new DemoShopPage();
        page.openDemoShopApp();
    }

    @AfterMethod
    public void cleanup() {
        System.out.println("Cleaning up after the test");
        Footer footer = new Footer();
        footer.resetPage();
    }

    @Test(dataProviderClass = UserDataProvider.class, dataProvider = "validUserDataProvider")
    @Severity(SeverityLevel.BLOCKER)
    @Description("A logged user performs checkout with one product.")
    public void logged_user_performs_checkout_with_one_product(User user) {
        page.getHeader().clickOnTheLoginButton();
        LoginModal loginModal = new LoginModal();
        loginModal.fillInUsername(user.getUsername());
        loginModal.fillInPassword(user.getPassword());
        loginModal.clickSubmitButton();

        Product incredibleConcreteHat = new Product("2", "Incredible Concrete Hat", "7.99");
        incredibleConcreteHat.addToCart();

        page.getHeader().clickOnTheCartIcon();
        CartPage cartPage = new CartPage();
        cartPage.clickOnTheCheckoutButton();
        CheckOutPage checkoutPage = new CheckOutPage();
        checkoutPage.fillInFirstName(user.getFirstName());
        checkoutPage.fillInLastName(user.getLastName());
        checkoutPage.fillInAddress(user.getAddress());
        checkoutPage.clickOnContinueCheckoutButton();
        checkoutPage.clickOnContinueCheckoutButton();

        String completeOrderMessage = checkoutPage.getCompleteOrderMessage();
        assertEquals(completeOrderMessage, "Thank you for your order!", "Complete order message is: " + completeOrderMessage);
    }

    @Test(dataProviderClass = UserDataProvider.class, dataProvider = "validUserDataProvider")
    @Severity(SeverityLevel.BLOCKER)
    @Description("A logged user performs checkout with two products.")
    public void logged_user_performs_checkout_with_two_products(User user) {
        page.getHeader().clickOnTheLoginButton();
        LoginModal loginModal = new LoginModal();
        loginModal.fillInUsername(user.getUsername());
        loginModal.fillInPassword(user.getPassword());
        loginModal.clickSubmitButton();

        Product incredibleConcreteHat = new Product("2", "Incredible Concrete Hat", "7.99");
        incredibleConcreteHat.addToCart();
        Product refinedFrozenMouse = new Product("0", "Refined Frozen Mouse", "9.99");
        refinedFrozenMouse.addToCart();

        page.getHeader().clickOnTheCartIcon();
        CartPage cartPage = new CartPage();
        cartPage.clickOnTheCheckoutButton();
        CheckOutPage checkoutPage = new CheckOutPage();
        checkoutPage.fillInFirstName(user.getFirstName());
        checkoutPage.fillInLastName(user.getLastName());
        checkoutPage.fillInAddress(user.getAddress());
        checkoutPage.clickOnContinueCheckoutButton();
        checkoutPage.clickOnContinueCheckoutButton();

        String completeOrderMessage = checkoutPage.getCompleteOrderMessage();
        assertEquals(completeOrderMessage, "Thank you for your order!", "Complete order message is: " + completeOrderMessage);
    }

}
