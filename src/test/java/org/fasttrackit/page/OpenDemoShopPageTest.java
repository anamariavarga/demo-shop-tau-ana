package org.fasttrackit.page;

import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.fasttrackit.DemoShopPage;
import org.fasttrackit.Footer;
import org.fasttrackit.config.BaseTestConfig;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.Test;

@Feature("Open page")
public class OpenDemoShopPageTest extends BaseTestConfig {
    @AfterMethod
    public void cleanup() {
        System.out.println("Cleaning up after the test");
        Footer footer = new Footer();
        footer.resetPage();
    }

   @Test
   @Severity(SeverityLevel.BLOCKER)
   @Description("The Demo Shop Page can be opened")
    public void open_Demo_Shop_Page() {
       DemoShopPage page;
        page = new DemoShopPage();
        page.openDemoShopApp();
    }
}
