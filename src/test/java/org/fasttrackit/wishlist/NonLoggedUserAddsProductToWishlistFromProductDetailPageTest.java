package org.fasttrackit.wishlist;

import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.fasttrackit.*;
import org.fasttrackit.config.BaseTestConfig;
import org.fasttrackit.data.provider.ProductsDataProvider;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

@Feature("Wishlist - Non Logged User")
public class NonLoggedUserAddsProductToWishlistFromProductDetailPageTest extends BaseTestConfig {
    DemoShopPage page;
    
    @BeforeMethod
    public void openDemoShopPage() {
        this.page = new DemoShopPage();
        page.openDemoShopApp();

    }

    @AfterMethod
    public void cleanup() {
        System.out.println("Cleaning up after the test");
        Footer footer = new Footer();
        footer.resetPage();
    }

    @Test(dataProviderClass = ProductsDataProvider.class, dataProvider = "productsDataProvider")
    @Severity(SeverityLevel.MINOR)
    @Description("Non logged user can successfully add product to wishlist")
    public void non_logged_user_adds_product_to_wishlist(Product product) {
        product.clickOnTheProductLink();
        ProductDetailsPage detailsPage = new ProductDetailsPage();
        detailsPage.clickOnTheAddToWishlistButton();

        String numberOfProductsInWishlist = page.getHeader().getNumberOfProductsInWishlist();
        assertEquals(numberOfProductsInWishlist, "1", "Adding one product to wishlist, the wishlist badge is 1.");
    }

    @Test(dataProviderClass = ProductsDataProvider.class, dataProvider = "productsDataProvider")
    @Severity(SeverityLevel.MINOR)
    @Description("Non logged user can successfully add two products to wishlist")
    public void non_logged_user_adds_two_products_to_wishlist(Product product) {
        Product awesomeGraniteChips = new Product("1", "Awesome Granite Chips", "15.99");
        awesomeGraniteChips.clickOnTheProductLink();
        ProductDetailsPage detailsPage = new ProductDetailsPage();
        detailsPage.clickOnTheAddToWishlistButton();

        Header header = new Header();
        header.clickOnTheShoppingBagIcon();
        product.clickOnTheAddToWishlistButton();

        String numberOfProductsInWishlist = page.getHeader().getNumberOfProductsInWishlist();
        assertEquals(numberOfProductsInWishlist, "2", "Adding two products to wishlist, the wishlist badge is 2.");
    }
}


