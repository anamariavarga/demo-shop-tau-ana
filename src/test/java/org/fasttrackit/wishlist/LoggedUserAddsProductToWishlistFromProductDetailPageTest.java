package org.fasttrackit.wishlist;

import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.fasttrackit.*;
import org.fasttrackit.config.BaseTestConfig;
import org.fasttrackit.data.provider.ProductsDataProvider;
import org.fasttrackit.data.provider.User;
import org.fasttrackit.data.provider.UserDataProvider;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

@Feature("Wishlist - Logged User")
public class LoggedUserAddsProductToWishlistFromProductDetailPageTest extends BaseTestConfig {
    DemoShopPage page;

    @BeforeMethod
    public void openDemoShopPage() {
        this.page = new DemoShopPage();
        page.openDemoShopApp();
    }

    @AfterMethod
    public void cleanup() {
        System.out.println("Cleaning up after the test");
        Footer footer = new Footer();
        footer.resetPage();
    }

    @Test(dataProviderClass = UserDataProvider.class, dataProvider = "validUserDataProvider")
    @Severity(SeverityLevel.MINOR)
    @Description("Various logged users can successfully add products to wishlist")
    public void various_logged_users_add_AwesomeGraniteChips_product_to_wishlist(User user) {
        page.getHeader().clickOnTheLoginButton();
        LoginModal loginModal = new LoginModal();
        loginModal.fillInUsername(user.getUsername());
        loginModal.fillInPassword(user.getPassword());
        loginModal.clickSubmitButton();

        Product awesomeGraniteChips = new Product("1", "Awesome Granite Chips", "15.99");
        awesomeGraniteChips.clickOnTheProductLink();
        ProductDetailsPage detailsPage = new ProductDetailsPage();
        detailsPage.clickOnTheAddToWishlistButton();

        String numberOfProductsInWishlist = page.getHeader().getNumberOfProductsInWishlist();
        assertEquals(numberOfProductsInWishlist, "1", "Adding one product to wishlist, the wishlist badge is 1.");
    }

    @Test(dataProviderClass = UserDataProvider.class, dataProvider = "validUserDataProvider")
    @Severity(SeverityLevel.MINOR)
    @Description("The logged user erase the unique product from the wishlist, the wishlist is empty.")
    public void various_logged_users_erase_AwesomeGraniteChips_from_inside_wishlist_validate_wishlist_has_no_badge(User user) {
        page.getHeader().clickOnTheLoginButton();
        LoginModal loginModal = new LoginModal();
        loginModal.fillInUsername(user.getUsername());
        loginModal.fillInPassword(user.getPassword());
        loginModal.clickSubmitButton();

        Product awesomeGraniteChips = new Product("1", "Awesome Granite Chips", "15.99");
        awesomeGraniteChips.clickOnTheProductLink();
        ProductDetailsPage detailsPage = new ProductDetailsPage();
        detailsPage.clickOnTheAddToWishlistButton();

        page.getHeader().clickOnTheWishlistIcon();
        WishlistPage wishlistPage = new WishlistPage();
        wishlistPage.clickOnTheRemoveFromWishlistButton();

        String numberOfProductsInWishlistFromWishlist = page.getWishlistPage().getNumberOfProductsInWishlistFromWishlist();
        assertEquals(numberOfProductsInWishlistFromWishlist, "", "Removing Awesome Granite Chips from wishlist. The wishlist has no badge.");
    }

    @Test(dataProviderClass = ProductsDataProvider.class, dataProvider = "productsDataProvider")
    @Severity(SeverityLevel.MINOR)
    @Description("The logged user dino can successfully add various products to wishlist")
    public void logged_user_dino_adds_various_products_to_wishlist(Product product) {
        page.getHeader().clickOnTheLoginButton();
        LoginModal loginModal = new LoginModal();
        loginModal.fillInUsername("dino");
        loginModal.fillInPassword("choochoo");
        loginModal.clickSubmitButton();

        product.clickOnTheProductLink();
        ProductDetailsPage detailsPage = new ProductDetailsPage();
        detailsPage.clickOnTheAddToWishlistButton();

        String numberOfProductsInWishlist = page.getHeader().getNumberOfProductsInWishlist();
        assertEquals(numberOfProductsInWishlist, "1", "Adding one product to wishlist, the wishlist badge is 1.");
    }
}


