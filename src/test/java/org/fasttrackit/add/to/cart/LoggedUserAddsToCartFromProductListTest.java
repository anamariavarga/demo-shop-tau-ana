package org.fasttrackit.add.to.cart;

import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.fasttrackit.*;
import org.fasttrackit.config.BaseTestConfig;
import org.fasttrackit.data.provider.User;
import org.fasttrackit.data.provider.UserDataProvider;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

@Feature("Add to cart - Logged User")
public class LoggedUserAddsToCartFromProductListTest extends BaseTestConfig {
    DemoShopPage page;

    @BeforeMethod
    public void openDemoShopPage() {
        this.page = new DemoShopPage();
        page.openDemoShopApp();
    }

    @AfterMethod
    public void cleanup() {
        System.out.println("Cleaning up after the test");
        Footer footer = new Footer();
        footer.resetPage();
    }

    @Test(dataProviderClass = UserDataProvider.class, dataProvider = "validUserDataProvider")
    @Severity(SeverityLevel.BLOCKER)
    @Description("The logged user can successfully add one product to cart")
    public void logged_user_adds_product_to_cart(User user) {
        page.getHeader().clickOnTheLoginButton();
        LoginModal loginModal = new LoginModal();
        loginModal.fillInUsername(user.getUsername());
        loginModal.fillInPassword(user.getPassword());
        loginModal.clickSubmitButton();

        Product metalMouse = new Product("7", "Practical Metal Mouse", "9.99");
        metalMouse.addToCart();

        boolean areProductsAdded = page.getHeader().areAddedProductsInCart();
        assertTrue(areProductsAdded, "Cart badge is displayed when products are added to cart.");
        String numberOfProductsInCart = page.getHeader().getNumberOfProductsInCart();
        assertEquals(numberOfProductsInCart, "1", "Logged in user adds one product to cart.");
    }

    @Test(dataProviderClass = UserDataProvider.class, dataProvider = "validUserDataProvider")
    @Severity(SeverityLevel.BLOCKER)
    @Description("The logged user can successfully add two products to cart")
    public void logged_user_adds_two_products_to_cart(User user) {
        page.getHeader().clickOnTheLoginButton();
        LoginModal loginModal = new LoginModal();
        loginModal.fillInUsername(user.getUsername());
        loginModal.fillInPassword(user.getPassword());
        loginModal.clickSubmitButton();

        Product licensedSteelGloves = new Product("8", "Licensed Steel Gloves", "14.99");
        licensedSteelGloves.addToCart();
        Product practicalWoodenBacon = new Product("4", "Practical Wooden Bacon", "29.99");
        practicalWoodenBacon.addToCart();

        boolean areProductsAdded = page.getHeader().areAddedProductsInCart();
        assertTrue(areProductsAdded, "Cart badge is displayed when products are added to cart.");
        String numberOfProductsInCart = page.getHeader().getNumberOfProductsInCart();
        assertEquals(numberOfProductsInCart, "2", "Logged in user adds two products to cart.");
    }

    @Test(dataProviderClass = UserDataProvider.class, dataProvider = "validUserDataProvider")
    @Severity(SeverityLevel.BLOCKER)
    @Description("The total cost of cart is correct")
    public void logged_users_use_the_cart_total_cost_is_correctly_added(User user) {
        page.getHeader().clickOnTheLoginButton();
        LoginModal loginModal = new LoginModal();
        loginModal.fillInUsername(user.getUsername());
        loginModal.fillInPassword(user.getPassword());
        loginModal.clickSubmitButton();

        Product licensedSteelGloves = new Product("8", "Licensed Steel Gloves", "14.99");
        licensedSteelGloves.addToCart();
        Product practicalWoodenBacon = new Product("4", "Practical Wooden Bacon", "29.99");
        practicalWoodenBacon.addToCart();

        page.getHeader().clickOnTheCartIcon();
        CartPage cartPage = new CartPage();
        double totalCartCost = cartPage.getTotalCartCostBasedOnProducts();
        double totalCartAmount = cartPage.getTotalCartAmount();
        assertEquals(totalCartCost, 44.98, "The total products is 44.98");
        assertEquals(totalCartAmount, 44.98, "The cart total is 44.98");
    }

}
