package org.fasttrackit.add.to.cart;

import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.fasttrackit.DemoShopPage;
import org.fasttrackit.Footer;
import org.fasttrackit.Product;
import org.fasttrackit.config.BaseTestConfig;
import org.fasttrackit.data.provider.ProductsDataProvider;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

@Feature("Add to cart - Non Logged User")
public class NonLoggedUserAddsToCartFromProductListTest extends BaseTestConfig {
    DemoShopPage page;

    @BeforeMethod
    public void openDemoShopPage() {
        this.page = new DemoShopPage();
        page.openDemoShopApp();

    }

    @AfterMethod
    public void cleanup() {
        System.out.println("Cleaning up after the test");
        Footer footer = new Footer();
        footer.resetPage();
    }

    @Test(dataProviderClass = ProductsDataProvider.class, dataProvider = "productsDataProvider")
    @Severity(SeverityLevel.BLOCKER)
    @Description("Non logged user can successfully adds one product to cart")
    public void non_logged_user_adds_one_product_to_cart(Product product) {
        product.addToCart();
        boolean areProductsAdded = page.getHeader().areAddedProductsInCart();
        assertTrue(areProductsAdded, "Cart badge is displayed when products are added to cart.");
        String numberOfProductsInCart = page.getHeader().getNumberOfProductsInCart();
        assertEquals(numberOfProductsInCart, "1", "Non logged user adds one product to cart.");
    }

    @Test(dataProviderClass = ProductsDataProvider.class, dataProvider = "productsDataProvider")
    @Severity(SeverityLevel.BLOCKER)
    @Description("Non logged user can successfully adds two products to cart")
    public void non_logged_user_adds_two_products_to_cart(Product product) {
        product.addToCart();
        product.addToCart();

        boolean areProductsAdded = page.getHeader().areAddedProductsInCart();
        assertTrue(areProductsAdded, "Cart badge is displayed when products are added to cart.");
        String numberOfProductsInCart = page.getHeader().getNumberOfProductsInCart();
        assertEquals(numberOfProductsInCart, "2", "Non logged user adds two products to cart.");
    }

}
