package org.fasttrackit.login;

import com.codeborne.selenide.Selenide;
import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.fasttrackit.DemoShopPage;
import org.fasttrackit.LoginModal;
import org.fasttrackit.config.BaseTestConfig;
import org.fasttrackit.data.provider.User;
import org.fasttrackit.data.provider.UserDataProvider;
import org.testng.annotations.*;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

@Feature("User login")
public class LoginTest extends BaseTestConfig {
    DemoShopPage page;

    @BeforeMethod
    public void setup() {
        page = new DemoShopPage();
        page.openDemoShopApp();
    }

    @AfterMethod
    public void reset() {
        Selenide.refresh();
        page.getFooter().resetPage();
    }

    @Test(dataProviderClass = UserDataProvider.class, dataProvider = "validUserDataProvider")
    @Severity(SeverityLevel.CRITICAL)
    @Description("The user can successfully login with username and password")
    public void user_can_login_on_Demo_Shop_Page(User user) {
        page.getHeader().clickOnTheLoginButton();
        LoginModal loginModal = new LoginModal();
        loginModal.fillInUsername(user.getUsername());
        loginModal.fillInPassword(user.getPassword());
        loginModal.clickSubmitButton();

        assertEquals(page.getHeader().getGreetingsElement(), user.getExpectedGreetingsMsg(), "Greetings message is: " + user.getExpectedGreetingsMsg());
    }

    @Test(dataProviderClass = UserDataProvider.class, dataProvider = "invalidUserDataProvider")
    @Severity(SeverityLevel.CRITICAL)
    @Description("The user cannot login with invalid user")
    public void user_cannot_login_on_Demo_Shop_Page_with_invalid_user(User user) {
        page.getHeader().clickOnTheLoginButton();
        LoginModal loginModal = new LoginModal();
        loginModal.fillInUsername(user.getUsername());
        loginModal.fillInPassword(user.getPassword());
        loginModal.clickSubmitButton();

        assertTrue(loginModal.isDisplayed());
        assertTrue(loginModal.isErrorMsgDisplayed(), "Login error message is displayed.");
        assertEquals(loginModal.getErrorMsg(), user.getErrorMessage());
        assertEquals(page.getHeader().getGreetingsElement(), user.getDefaultGreetingsMsg(), "Greetings message is: " + user.getDefaultGreetingsMsg());

    }

    @Test
    @Severity(SeverityLevel.CRITICAL)
    @Description("The user cannot login without data")
    public void user_cannot_login_on_Demo_Shop_Page_without_data() {
        page.getHeader().clickOnTheLoginButton();
        LoginModal loginModal = new LoginModal();
        loginModal.clickSubmitButton();

        assertTrue(loginModal.isDisplayed());
        assertTrue(loginModal.isErrorMsgDisplayed(), "Login error message is displayed.");

        loginModal.clickOnCloseModalButton();

        String closeModal = page.getHeader().getGreetingsElement();
        assertEquals(closeModal, "Hello guest!", "Validate that the modal is closed.");
    }

    @Test
    @Severity(SeverityLevel.CRITICAL)
    @Description("The login modal can be opened and closed.")
    public void open_and_close_the_login_modal_on_Demo_Shop_Page() {
        page.getHeader().clickOnTheLoginButton();
        LoginModal loginModal = new LoginModal();
        loginModal.clickOnCloseModalButton();

        String closeModal = page.getHeader().getGreetingsElement();
        assertEquals(closeModal, "Hello guest!", "Validate that the modal is closed.");
    }
}


