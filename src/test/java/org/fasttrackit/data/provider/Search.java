package org.fasttrackit.data.provider;

public class Search {
    private final String searchWords;

    public Search(String searchWords) {
        this.searchWords = searchWords;
    }

    public String getSearchWords() {
        return searchWords;
    }

    @Override
    public String toString() {
        return "Test runs with: " + searchWords;
    }
}
