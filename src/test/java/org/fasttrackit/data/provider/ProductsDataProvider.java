package org.fasttrackit.data.provider;

import org.fasttrackit.Product;
import org.testng.annotations.DataProvider;

public class ProductsDataProvider {
    @DataProvider(name = "productsDataProvider")
    public Object[][] feedProductsDataProvider() {

        Product refinedFrozenMouse = new Product("0", "Refined Frozen Mouse", "9.99");
        Product awesomeGraniteChips = new Product("1", "Awesome Granite Chips", "15.99");
        Product incredibleConcreteHat = new Product("2", "Incredible Concrete Hat", "7.99");
        Product awesomeMetalChair = new Product("3", "Awesome Metal Chair", "15.99");
        Product practicalWoodenBacon = new Product("4", "Practical Wooden Bacon", "29.99");
        Product awesomeSoftShirt = new Product("5", "Awesome Soft Shirt", "29.99");
        Product practicalWoodenBacon1 = new Product("6", "Practical Wooden Bacon", "1.99");
        Product metalMouse = new Product("7", "Practical Metal Mouse", "9.99");
        Product licensedSteelGloves = new Product("8", "Licensed Steel Gloves", "14.99");
        Product gorgeousSoftPizza = new Product("9", "Gorgeous Soft Pizza", "19.99");

        return new Object[][] {
                {refinedFrozenMouse},
                {awesomeGraniteChips},
                {incredibleConcreteHat},
                {awesomeMetalChair},
                {practicalWoodenBacon},
                {awesomeSoftShirt},
                {practicalWoodenBacon1},
                {metalMouse},
                {licensedSteelGloves},
                {gorgeousSoftPizza}
        };
    }
}
