package org.fasttrackit.data.provider;

public class User {
    private final String username;
    private final String password;
    private String firstName;
    private String lastName;
    private String address;
    private final String expectedGreetingsMsg;
    private final String defaultGreetingsMsg = "Hello guest!";
    private String errorMessage = "";

    public User(String username, String password, String firstName, String lastName, String address){
        this.username = username;
        this.password = password;
        this.expectedGreetingsMsg = "Hi " + username + "!";
        this.firstName = firstName;
        this.lastName = lastName;
        this.address = address;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }
    public String getFirstName(){return firstName;}
    public String getLastName(){return lastName;}
    public String getAddress(){return address;}

    public String getExpectedGreetingsMsg() {
        return expectedGreetingsMsg;
    }
    public String getDefaultGreetingsMsg() {
        return defaultGreetingsMsg;
    }

    @Override
    public String toString(){
        return "Test runs with: " + username;
    }
}
