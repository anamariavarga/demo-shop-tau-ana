package org.fasttrackit.data.provider;

import org.testng.annotations.DataProvider;

public class UserDataProvider {

    @DataProvider(name = "validUserDataProvider")
    public Object[][] feedUserDataProvider() {
        User beetleUser = new User("beetle", "choochoo", "Evelin", "Guttenberg", "Germany, Fussen, Morgen Strasse 29");
        User dinoUser = new User("dino", "choochoo", "Emily", "Basset", "Canada, Montreal, Hipper street 69");
        User turtleUser = new User("turtle", "choochoo", "Mark", "Sorriso","Spain, Barcelona, Diagonal 59" );

        return new Object[][] {
                {beetleUser},
                {dinoUser},
                {turtleUser}
        };
    }
    @DataProvider(name = "invalidUserDataProvider")
    public Object[][] feedInvalidUserDataProvider() {
        User lockedUser = new User("locked", "choochoo","Locked", "Guttenberg", "Germany, Fussen, Morgen Strasse 29");
        lockedUser.setErrorMessage("The user has been locked out.");

        User invalidPass = new User("beetle", "choochoo1", "Emily", "Bassets", "Canada, Montreal, Hipper street 69");
        invalidPass.setErrorMessage("Incorrect username or password!");

        User invalidUser = new User("test", "choochoo", "Mark", "Sorriso","Spain, Barcelona, Diagonal 58");
        invalidUser.setErrorMessage("Incorrect username or password!");

        return new Object[][] {
                {lockedUser},
                {invalidPass},
                {invalidUser}
        };
    }}



