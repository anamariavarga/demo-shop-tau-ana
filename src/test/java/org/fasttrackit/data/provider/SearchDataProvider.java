package org.fasttrackit.data.provider;

import org.testng.annotations.DataProvider;

public class SearchDataProvider {

    @DataProvider(name = "searchDataProvider")
    public static Object[][] searchDataProvider() {
        Search woodSearch = new Search("wood");
        Search metalSearch = new Search("metal");
        Search mouseSearch = new Search("mouse");
        Search licensedSearch = new Search("licensed");
        Search baconSearch = new Search("bacon");

        return new Object[][]{
                {woodSearch},
                {metalSearch},
                {mouseSearch},
                {licensedSearch},
                {baconSearch},
        };
    }
}



