package org.fasttrackit.products;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Description;
import io.qameta.allure.Feature;
import io.qameta.allure.Severity;
import io.qameta.allure.SeverityLevel;
import org.fasttrackit.DemoShopPage;
import org.fasttrackit.Footer;
import org.fasttrackit.LoginModal;
import org.fasttrackit.config.BaseTestConfig;
import org.fasttrackit.data.provider.Search;
import org.fasttrackit.data.provider.SearchDataProvider;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.Assert.assertTrue;

@Feature("Search - Non Logged User")
public class NonLoggedUserSearchProductsBySearchingWordsTest extends BaseTestConfig {
    DemoShopPage page = new DemoShopPage();

    @BeforeClass
    public void openDemoShopPage() {
        page.openDemoShopApp();
    }

    @AfterMethod
    public void cleanup() {
        System.out.println("Cleaning up after the test");
        Footer footer = new Footer();
        footer.resetPage();
    }

    @Test(dataProviderClass = SearchDataProvider.class, dataProvider = "searchDataProvider")
    @Severity(SeverityLevel.CRITICAL)
    @Description("The logged user can search in the product list")
    public void non_logged_user_search_in_the_product_list(Search search) {
        page.clickOnSearchField();
        String searchWords = search.getSearchWords();
        page.fillInSearchField(searchWords);
        page.clickOnTheSearchButton();
        ElementsCollection products = page.getProducts();
        System.out.println(products);
        for (SelenideElement product : products) {
            assertTrue(product.text().toLowerCase().contains(searchWords.toLowerCase()));
        }
    }
}