package org.fasttrackit;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class DemoShopPage extends Page {
    private Header header;
    private Footer footer;

    private SelenideElement searchField = $("#input-search");
    private SelenideElement searchButton = $(".btn-light");
    private SelenideElement sortField = $(".col-md-auto .sort-products-select");
    private final SelenideElement sortByNameAZ = $("option:nth-child(1)");
    private final SelenideElement sortByNameZA = $("option:nth-child(2)");
    private final SelenideElement sortByPriceLowToHigh = $("option:nth-child(3)");
    private final SelenideElement sortByPriceHighToLow = $("option:nth-child(4)");
    private final SelenideElement pricesList = $("div.text-muted.text-center.card-footer > p:nth-child(1) > span");
    ElementsCollection products = $$(".card");

    public DemoShopPage() {
        this.header = new Header();
        this.footer = new Footer();
    }

    public ElementsCollection getProducts() {
        return products;
    }

    public ElementsCollection getPrices() {
        return getPrices();
    }


    public Footer getFooter() {
        return footer;
    }

    public Header getHeader() {
        return header;
    }

    public WishlistPage getWishlistPage() {
        return new WishlistPage();
    }

    public void fillInSearchField(String searchWords) {
        searchField.sendKeys(searchWords);
    }

    public void clickOnSearchField() {
        searchField.click();
    }

    public void clickOnTheSearchButton() {
        searchButton.click();
    }

    public void clickOnSortField(CharSequence sortCriteria) {
        sortField.sendKeys(sortCriteria);
    }

    public void fillInSortField(String sortCriteria) {
        sortField.sendKeys(sortCriteria);
    }

}

