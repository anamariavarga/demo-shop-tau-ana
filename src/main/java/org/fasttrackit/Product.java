package org.fasttrackit;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class Product {

    private final SelenideElement productCard;
    private final SelenideElement productLink;
    private final SelenideElement addToCartButton;
    private final SelenideElement addToWishlistButton;
    private SelenideElement searchedProducts = $(".row .card");

    public SelenideElement getSearchedProducts() {
        return searchedProducts;
    }

    private String name;
    private String price;

    public SelenideElement getProductCard() {
        return productCard;
    }

    public Product(String productId, String name, String price) {
        String productSelector = String.format("[href='#/product/%s']", productId);
        this.productLink = $(productSelector);
        this.productCard = this.productLink.parent().parent();
        this.addToCartButton = productCard.$(".fa-cart-plus");
        this.name = name;
        this.price = price;
        this.addToWishlistButton = productCard.$(".btn-link .fa-heart");
    }

    public String getName() {
        return name;
    }

    public String getPrice() {
        return price;
    }

    public String getProductLink() {
        return String.valueOf(productLink);
    }

    public void clickOnTheProductLink() {
        productLink.click();
    }

    public void clickOnTheAddToWishlistButton() {
        addToWishlistButton.click();
    }

    public void addToCart() {
        System.out.println("Clicked on the " + addToCartButton + " on " + name);
        this.addToCartButton.scrollTo();
        this.addToCartButton.click();
    }

    @Override
    public String toString() {
        return "Test runs with: " + name;
    }

}
