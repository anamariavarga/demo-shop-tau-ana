package org.fasttrackit;

import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$$;

public class Header {
    private SelenideElement loginButton = $("[data-icon=sign-in-alt]");
    private final SelenideElement cartIcon = $(".fa-shopping-cart");
    private SelenideElement cartBadge = $(".shopping_cart_badge");
    private ElementsCollection cartBadges = $$(".shopping_cart_badge");
    private SelenideElement whishlistIcon = $(".fa-heart");
    private SelenideElement wishlistBadge = $(".fa-layers-counter");
    private SelenideElement greetingsElement = $(".navbar-text span span");
    private SelenideElement greetingsPersonalizedMsg = $("[href='#/account']");
    private SelenideElement shoppingBagIcon = $(".navbar-brand");

    public Header() {

    }

    public String getNumberOfProductsInCart() {
        return this.cartBadge.text();
    }

    public boolean areAddedProductsInCart() {
        return cartBadges.size() > 0;
    }

    public String getNumberOfProductsInWishlist() {
        return this.wishlistBadge.text();
    } //ana

    public String getWelcomeMessage() {
        return this.greetingsPersonalizedMsg.text();
    }

    public String getGreetingsElement() {
        return greetingsElement.text();
    }

    public void clickOnTheWishlistIcon() { //ana
        whishlistIcon.click();
    }

    public void clickOnTheLoginButton() {
        loginButton.click();
    }


    public void clickOnTheCartIcon() {
        cartIcon.click();
    }

    public void clickOnTheShoppingBagIcon() {
        shoppingBagIcon.click();
    }

    public String getNumberOfProductsInWishlistFromWishlist() {
        return null;
    }

    public String getGreetingsMsg() {
        return greetingsElement.text();
    }

}
