package org.fasttrackit;

import java.util.List;

public class Main {
    public static void main(String[] args) {

        DemoShopPage page = new DemoShopPage();
        page.openDemoShopApp();
        String greetingsMsg = page.getHeader().getGreetingsMsg();
        System.out.println("Validate that the greetings msg is: " + greetingsMsg);
        page.getHeader().clickOnTheLoginButton();
        LoginModal modal = new LoginModal();
        modal.fillInUsername("beetle");
        modal.fillInPassword("choochoo");
        modal.clickSubmitButton();
        Header header = new Header();
        String greetingsMessage = header.getGreetingsMsg();
        System.out.println("Validate that the greetings msg is: " + greetingsMessage);

        System.out.println("-----------------------------------------------------------------------");

        CartPage cartDetails = new CartPage();

        Product metalMouseProduct = new Product("9,", "Practical Metal Mouse", "9.99");

        page.getHeader().clickOnTheCartIcon();
        List<Product> productsInCart = cartDetails.getProductInCart();
        System.out.println("Verify that the product one metal mouse is in the cart: " + productsInCart.get(0).getName());
        System.out.println("Verify that the product practical metal mouse price is 9.99$: " + cartDetails.getProductInCart().get(0).getPrice() + "$");
        System.out.println("Verify that the total price is 9.99$: " + cartDetails.getTotalCartCostBasedOnProducts() + "$");

        page.refresh();
        Product metalMouse2ndProduct = new Product("9", "Practical Metal Mouse", "9.99");
        metalMouseProduct.addToCart();
        Product softPizza = new Product("7", "Gorgeous soft pizza", "19.99");
        CartPage cart2ProductsDetails = new CartPage();
        page.getHeader().clickOnTheCartIcon();
        System.out.println("Products in cart: " + cart2ProductsDetails.getNumberOfDistinctProducts());
    }

}

