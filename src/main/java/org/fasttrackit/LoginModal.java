package org.fasttrackit;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class LoginModal {
    private SelenideElement userField = $("#user-name");
    private SelenideElement passwordField = $("#password");
    private SelenideElement submitButton = $("[type=submit]");
    private SelenideElement modal = $(".modal-content");
    private SelenideElement errorMessage = $(".error");

    private SelenideElement closeModalButton = $(".modal-header .close");

    public void fillInUsername(String user) {
        userField.sendKeys(user);
    }

    public void fillInPassword(String password) {
        passwordField.sendKeys(password);
    }

    public void clickSubmitButton() {
        submitButton.click();
    }

    public void clickOnCloseModalButton() {
        closeModalButton.click();
    }

    public boolean isErrorMsgDisplayed() {
        return this.errorMessage.exists() && this.errorMessage.isDisplayed();
    }

    public String getErrorMsg() {
        return errorMessage.text();
    }

    public boolean isDisplayed() {
        return this.modal.exists() && this.modal.isDisplayed();
    }
}