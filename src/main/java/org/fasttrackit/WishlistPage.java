package org.fasttrackit;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class WishlistPage {
    private final SelenideElement removeFromWishlistButton = $(".fa-heart-broken");
    private SelenideElement wishlistBadgeFromWishlist = $(".fa-layers");

    public void clickOnTheRemoveFromWishlistButton() {
        this.removeFromWishlistButton.click();
    }

    public WishlistPage() {
        this.wishlistBadgeFromWishlist = wishlistBadgeFromWishlist;
    }

    public String getNumberOfProductsInWishlistFromWishlist() {
        return this.wishlistBadgeFromWishlist.text();
    }
}
