package org.fasttrackit;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class ProductDetailsPage {
    private final SelenideElement addToCartButton = $(".fa-cart-plus");
    private final SelenideElement addToWishlistButton = $(".row .fa-heart");

    public void clickOnTheAddToCartButton() {
        this.addToCartButton.click();
    }

    public void clickOnTheAddToWishlistButton() {
        this.addToWishlistButton.click();
    }
}
