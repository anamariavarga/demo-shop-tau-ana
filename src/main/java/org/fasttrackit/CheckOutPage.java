package org.fasttrackit;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Selenide.$;

public class CheckOutPage {
    private SelenideElement firstNameField = $("#first-name");
    private SelenideElement lastNameField = $("#last-name");
    private SelenideElement addressField = $("#address");
    private SelenideElement continueCheckoutButton = $(".btn-success");

    private SelenideElement completeOrderMessage = $(".text-center");

    private final SelenideElement errorMessage = $(".error");
    private final SelenideElement cancelButton = $(".btn-danger");

    public void fillInFirstName(String user) {
        firstNameField.sendKeys(user);
    }

    public void fillInLastName(String user) {
        lastNameField.sendKeys(user);
    }

    public void fillInAddress(String user) {
        addressField.sendKeys(user);
    }

    public void clickOnContinueCheckoutButton() {
        continueCheckoutButton.click();
    }

    public void clickOnCancelButton() {
        cancelButton.click();
    }

    public String getCompleteOrderMessage() {
        completeOrderMessage.text();
        return completeOrderMessage.text();
    }

    public String getErrorMessage() {
        errorMessage.text();
        return errorMessage.text();
    }
}
